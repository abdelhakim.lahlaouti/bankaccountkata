package service;

import entities.Account;
import entities.Operation;
import exception.InvalidAmountException;
import exception.NotEnoughBalanceException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static service.OperationsDisplayService.getOperationsBetween;

public class BankAccountServiceImplTest {

    private Account account;
    private BankAccountService bankAccountService;
    private static final double OVERDRAFT = 200;

    @Before
    public void setUp() {
        //given
        account = new Account();
        account.setOverdraft(OVERDRAFT);
        bankAccountService = new BankAccountServiceImpl();
    }

    private BigDecimal stringToBigDecimal (String value) {
        return new BigDecimal(value);
    }

    @Test
    public void shouldInitializeAccountBalance() {
        assertEquals(stringToBigDecimal("0"),account.getBalance());
    }

    @Test
    public void shouldDeposit500() {
        //when
        bankAccountService.deposit(account,"500");

        //then
        assertEquals(stringToBigDecimal("500"),account.getBalance());
    }

    @Test
    public void shouldDepositMultiplesTimes() {
        //when
        bankAccountService.deposit(account,"500");
        bankAccountService.deposit(account,"78.56");

        //then
        assertEquals(stringToBigDecimal("578.56"),account.getBalance());
    }

    @Test
    public void shouldWithdraw(){
        //when
        bankAccountService.deposit(account,"500");
        bankAccountService.withdraw(account,"368.23");

        //then
        assertEquals(stringToBigDecimal("131.77"),account.getBalance());

    }

    @Test
    public void shouldWithDrawMultiplesTimes() {
        //when
        bankAccountService.deposit(account,"500");

        bankAccountService.withdraw(account,"180");
        bankAccountService.withdraw(account,"23");

        //then
        assertEquals(stringToBigDecimal("297"),account.getBalance());
    }

    @Test (expected = InvalidAmountException.class)
    public void shouldGetInvalidAmountExceptionInDeposit() {
        //when
        bankAccountService.deposit(account,"0");
    }

    @Test (expected = InvalidAmountException.class)
    public void shouldGetInvalidAmountExceptionInWithdrawal() {
        //when
        bankAccountService.withdraw(account,"-125");
    }

    @Test(expected = NotEnoughBalanceException.class)
    public void shouldGetNotEnoughBalanceException() {
        //when
        bankAccountService.deposit(account,"500");
        bankAccountService.withdraw(account,"800");
    }
//TODO: Test clock too with mock
    @Test
    public void shouldGetTransactions() {

        //when
        bankAccountService.deposit(account,"500");
        bankAccountService.deposit(account,"78.56");
        bankAccountService.withdraw(account,"180");

        //then
        assertEquals(3,account.getOperations().size());

    }

    @Test
    public void shouldGetTransactionsBetweenTwoDates() {

        //when
        bankAccountService.deposit(account,"500");
        bankAccountService.deposit(account,"78.56");
        bankAccountService.withdraw(account,"180");
        bankAccountService.deposit(account,"500");
        bankAccountService.withdraw(account,"20");

        account.getOperations().get(0).setDateTime(LocalDateTime.of(2019, Month.APRIL, 1, 10, 10, 30));
        account.getOperations().get(2).setDateTime(LocalDateTime.of(2019, Month.MAY, 2, 10, 10, 30));
        account.getOperations().get(3).setDateTime(LocalDateTime.of(2019, Month.JUNE, 30, 10, 10, 30));
        account.getOperations().get(4).setDateTime(LocalDateTime.of(2019, Month.AUGUST, 9, 10, 10, 30));

        LocalDate fromDate = LocalDate.of(2019,Month.APRIL,1);
        LocalDate toDate = LocalDate.of(2019,Month.JUNE,30);

        List<Operation> operationsBetween = getOperationsBetween(account.getOperations(),fromDate ,toDate);

        //then
        assertEquals(3,operationsBetween.size());
    }
}