import entities.Account;
import service.BankAccountService;
import service.BankAccountServiceImpl;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import static service.OperationsDisplayService.*;

public class Main {

    public static void main(final String[] args)  {
        final Account account = Account.of("1000");
        final BankAccountService bankAccount = new BankAccountServiceImpl();

        bankAccount.deposit(account,"205.2");
        bankAccount.deposit(account,"50.576");
        bankAccount.withdraw(account,"20.366");
        bankAccount.deposit(account,"10.81");


        displayOperations(account.getOperations());


        account.getOperations().get(0).setDateTime(LocalDateTime.of(2019, Month.APRIL, 1, 10, 10, 30));
        account.getOperations().get(3).setDateTime(LocalDateTime.of(2019, Month.JUNE, 30, 10, 10, 30));

        LocalDate fromDate = LocalDate.of(2019, Month.APRIL,1);
        LocalDate toDate = LocalDate.of(2019,Month.JUNE,30);

        System.out.println("Operations Between 1 April 2019 and 30 June 2019");

        displayOperationsBetweenTwoDates(account.getOperations(),fromDate,toDate);

    }
}
