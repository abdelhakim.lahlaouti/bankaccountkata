package utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

//just for the display on the console
public class MonetaryFormat {
    private MonetaryFormat() {
    }

    public static BigDecimal getMonetaryFormat (BigDecimal amount) {
        return amount.setScale(2, RoundingMode.HALF_EVEN);
    }
}
