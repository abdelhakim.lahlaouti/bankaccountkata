package entities;

import enumeration.OperationTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static utils.MonetaryFormat.getMonetaryFormat;

@Data
@AllArgsConstructor
public class Operation {

    private static final String FIELD_SEPARATOR = ";\t\t";

    private LocalDateTime dateTime;
    private BigDecimal amount;
    private BigDecimal newBalance;
    private OperationTypeEnum operationType;

    public void display() {
        StringBuilder operationDisplay = new StringBuilder();
         operationDisplay.append(dateTime)
                .append(FIELD_SEPARATOR)
                .append(operationType)
                .append(FIELD_SEPARATOR)
                .append(getMonetaryFormat(amount))
                .append(FIELD_SEPARATOR)
                .append(getMonetaryFormat(newBalance))
                .append(FIELD_SEPARATOR);
        System.out.println(operationDisplay.toString());
    }

}
