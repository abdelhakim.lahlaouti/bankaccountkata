package entities;

import lombok.Data;

import java.math.BigDecimal;
import java.util.LinkedList;

@Data
public class Account {
    private BigDecimal balance;
    // LinkedList because add operations are expected to be more frequent then gets
    private LinkedList<Operation> operations;
    private double overdraft;

    private Account(BigDecimal balance) {
        this.balance = balance;
        this.operations = new LinkedList<>();
    }

    public Account() {
        this.balance = BigDecimal.ZERO;
        this.operations = new LinkedList<>();
    }

    public static Account of (String value) {
        return new Account(new BigDecimal(value));
    }
    public static Account of (BigDecimal value) {
        return new Account(value);
    }





}
