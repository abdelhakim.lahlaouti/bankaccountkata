package service;


import entities.Account;
import entities.Operation;
import enumeration.OperationTypeEnum;
import exception.InvalidAmountException;
import exception.NotEnoughBalanceException;

import java.math.BigDecimal;
import java.time.LocalDateTime;


public class BankAccountServiceImpl implements BankAccountService {

    @Override
    public Account deposit(Account account, String amount) {
        checkAmount(new BigDecimal(amount));
        BigDecimal newBalance = account.getBalance().add(new BigDecimal(amount));
        account.setBalance(newBalance);
        account.getOperations().add(new Operation(LocalDateTime.now(),new BigDecimal(amount),newBalance, OperationTypeEnum.DEPOSIT));
        return account;
    }

    @Override
    public Account withdraw(Account account, String amount) {
        checkAmount(new BigDecimal(amount));
        checkBalanceIsSufficient(new BigDecimal(amount),account.getBalance(),account.getOverdraft());
        BigDecimal newBalance = account.getBalance().subtract(new BigDecimal(amount));
        account.setBalance(newBalance);
        account.getOperations().add(new Operation(LocalDateTime.now(),new BigDecimal(amount),newBalance, OperationTypeEnum.WITHDRAWAL));
        return account;
    }

    private void checkAmount(BigDecimal amount) {
        if(amount.compareTo(BigDecimal.ZERO) <= 0)
            throw new InvalidAmountException();
    }

    private void checkBalanceIsSufficient(BigDecimal amount, BigDecimal balance, double overdraft) {
        if(amount.compareTo(balance.add(BigDecimal.valueOf(overdraft))) > 0)
            throw new NotEnoughBalanceException();
    }


}
