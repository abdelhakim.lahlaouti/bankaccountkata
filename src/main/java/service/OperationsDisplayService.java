package service;

import entities.Operation;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;


public class OperationsDisplayService {

	private static final String HEAD = "DATE\t\t\t\t\t\t\tTYPE\t\t\tAMOUNT\t\tBALANCE";

	private OperationsDisplayService() {
	}

	static List<Operation> getOperationsBetween(List<Operation> operations, LocalDate fromDate, LocalDate toDate) {
		return operations.stream()
				.filter(operation ->
						(operation.getDateTime().toLocalDate().isAfter(fromDate) ||  operation.getDateTime().toLocalDate().isEqual(fromDate))
								&& operation.getDateTime().toLocalDate().isBefore(toDate))
				.collect(Collectors.toList());
	}


	public static void displayOperations( List<Operation> operations) {

		System.out.println(HEAD);
		operations.forEach(Operation::display);
	}

	public static void displayOperationsBetweenTwoDates(List<Operation> operations, LocalDate fromDate, LocalDate toDate) {
		System.out.println(HEAD);
		getOperationsBetween(operations,fromDate,toDate).forEach(Operation::display);
	}

}
