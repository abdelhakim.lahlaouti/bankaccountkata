package service;

import entities.Account;

public interface BankAccountService {
    Account deposit (Account account, String amount) ;
    Account withdraw(Account account, String amount) ;
}
